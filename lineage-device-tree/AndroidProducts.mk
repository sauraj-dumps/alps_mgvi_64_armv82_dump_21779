#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_k6789v1_64.mk

COMMON_LUNCH_CHOICES := \
    lineage_k6789v1_64-user \
    lineage_k6789v1_64-userdebug \
    lineage_k6789v1_64-eng
