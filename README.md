## sys_mssi_64_cn_armv82-user 13 TP1A.220624.014 mp1V1110 release-keys
- Manufacturer: alps
- Platform: common
- Codename: mgvi_64_armv82
- Brand: alps
- Flavor: sys_mssi_64_cn_armv82-user
- Release Version: 13
- Kernel Version: 5.10.149
- Id: TP1A.220624.014
- Incremental: mp1V1110
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: alps/hal_mgvi_64_armv82/mgvi_64_armv82:12/SP1A.210812.016/mp1V1502:user/release-keys
- OTA version: 
- Branch: sys_mssi_64_cn_armv82-user-13-TP1A.220624.014-mp1V1110-release-keys
- Repo: alps_mgvi_64_armv82_dump_21779
